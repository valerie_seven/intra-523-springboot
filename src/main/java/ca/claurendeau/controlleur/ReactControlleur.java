package ca.claurendeau.controlleur;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ca.claurendeau.domaine.Message;


@RestController
public class ReactControlleur {
	
    @RequestMapping(value="/message", method=RequestMethod.GET)
    public String message() {
        return new Message().getMESSAGE();
    }
}
