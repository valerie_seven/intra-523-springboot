package ca.claurendeau.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.claurendeau.Intra523Application;
import ca.claurendeau.domaine.Person;
import ca.claurendeau.repository.PersonRepository;
@Service
public class DemoService {
	private static final Logger log = LoggerFactory.getLogger(Intra523Application.class);
	
	@Autowired
	private PersonRepository repository;
	
	public void createInitialPersonnes() {
		// Save some Person
		repository.save(new Person("bob","bob@intra.ca"));
		repository.save(new Person("john","john@intra.ca"));
		repository.save(new Person("leila","leila@intra.ca"));
		
		// fetch all Person
        log.info("Message found with findAll():");
        log.info("-------------------------------");
        for (Person person : repository.findAll()) {
            log.info(person.toString());
        }
        log.info("");
	}
}
