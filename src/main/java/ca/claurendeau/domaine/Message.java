package ca.claurendeau.domaine;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="MESSAGE")
public class Message {
    
	@GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    @Id
    private Long id = -1l;
	
	private final String MESSAGE = "Cherche un nom";
	
	public Message() {
		super();
	}

	public String getMESSAGE() {
		return MESSAGE;
	}
	
}
