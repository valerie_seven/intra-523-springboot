package ca.claurendeau.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.claurendeau.domaine.Message;


public interface MessageRepository extends JpaRepository<Message, Long> {
    
    List<Message> findBy(String MESSAGE);

}
